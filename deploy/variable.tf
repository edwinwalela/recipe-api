variable "prefix" {
  default = "recipe-api"
}

variable "project" {
  default = "recipe-api" # name of repo
}

variable "contact" {
  default = "dev@mail.com" # contact person
}

variable "db_username" {
  description = "RDS Postgres DB username"
}

variable "db_password" {
  description = "RDS Postgres DB password"
}

variable "bastion_key_name" {
  # name of key pair created on ec2>key pairs
  default = "recipe-app-api-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "173130437624.dkr.ecr.sa-east-1.amazonaws.com/recipe-app-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR img for proxy"
  default     = "173130437624.dkr.ecr.sa-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  # Set up in Gitlab Project
  description = "Secret key for Django app"
}