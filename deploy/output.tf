# see database internal address (will be used in configuring bastion)
output "db_host" {
  value = aws_db_instance.main.address
}

# see public dns name for bastion host so that we can connect to it
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

# see lb dns name
output "api_endpoint" {
  value = aws_lb.api.dns_name
}