
# add multiple subnets to your DB
resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

# controll inbound and outbound access allowed to a resource
resource "aws_security_group" "rds" {
  description = "Allow access to RDS db instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  # ingress -> inbound access rules
  # egress -> outbound access rules
  # allow inbound access within vpc on port 5432 (DB)
  # outbound access not needed since db is private
  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    # access limited to resources that have the following security groups
    security_groups = [
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id # allow ecs to access db
    ]
  }

  tags = local.common_tags
}

# creates rds instance
resource "aws_db_instance" "main" {
  identifier              = "${local.prefix}-db"
  name                    = "recipe" # name of database in postgres
  allocated_storage       = 20       # GB
  storage_type            = "gp2"    # ssd storage (entry level storage types)
  engine                  = "postgres"
  engine_version          = "13.4" # pg version
  instance_class          = "db.t3.micro"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0     # days to maintain backups for db
  multi_az                = false # run on multiple availability zones
  skip_final_snapshot     = true
  vpc_security_group_ids = [
    aws_security_group.rds.id
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}