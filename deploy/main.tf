terraform {
  # configure where to store TF state
  backend "s3" {
    bucket         = "edwin-recipe-app-api-tfstate" # bucket name
    key            = "recipe-app.tfstate"           # where file is stored (path) (first run will create this file)
    region         = "sa-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-tf-state-lock" # name of dynamoDB table
  }
}

provider "aws" {
  region  = "sa-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# resource we can use to access region we are applying terraform to
data "aws_region" "current" {} 