# bastion is a EC2 instance (a vps)

# "data" -> retrieving info from AWS

# image (OS) to create the EC2
data "aws_ami" "amazon_linux" {
  most_recent = true
  # get ami with specified name
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"] # official amazon images
}

# Role
resource "aws_iam_role" "bastion" {
  name = "${local.prefix}-bastion"
  #policy to allow ec2 to assume a role
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

# Attach policy to role
resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role = aws_iam_role.bastion.name
  # attach policy to our iam role defined above
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

resource "aws_instance" "bastion" {
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = "t2.micro"
  user_data            = file("./templates/bastion/user-data.sh")
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  key_name             = var.bastion_key_name
  # vpc where bastion will launch into (public in order to be accessible from local machine)
  subnet_id = aws_subnet.public_a.id

  # attach secuirty group to bastion
  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]

  # combine common tags with an extra tag
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound & outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  # control inbound internet access
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"] # allows access from any ip address
  }

  # control outbound internet access from bastion to the internet
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }
  # control outbound internet access from bastion to the internet
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }


  # control outbound access from bastion to postgress
  egress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  tags = local.common_tags

}