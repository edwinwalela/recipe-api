# Setup ECS cluster

resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"
  tags = local.common_tags
}

# policy
resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving of images and adding to logs"
  # task execution role - allows ECS task to retrieve image from ECR and put logs into log stream
  policy = file("./templates/ecs/task-exec-role.json")
}

# role 
resource "aws_iam_role" "task_execution_role" {
  name = "${local.prefix}-task-exec-role"
  # assume role - allow ecs task to assume role above
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

# attach policy to role
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

# giving permission that running docker containers needs during runtime
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")
  tags               = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"
  tags = local.common_tags
}

# template for container definition
data "template_file" "api_container_definitions" {
  template = file("./templates/ecs/container-definitions.json.tpl")

  # populate ^ templates vars 
  vars = {
    app_image         = var.ecr_image_api
    proxy_image       = var.ecr_image_proxy
    django_secret_key = var.django_secret_key
    db_host           = aws_db_instance.main.address
    db_name           = aws_db_instance.main.name
    db_user           = aws_db_instance.main.username
    db_pass           = aws_db_instance.main.password
    log_group_name    = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region  = data.aws_region.current.name
    allowed_hosts     = aws_lb.api.dns_name
  }
}

# task definition for API
resource "aws_ecs_task_definition" "api" {
  family                   = "${local.prefix}-api"                                 # name of task definition
  container_definitions    = data.template_file.api_container_definitions.rendered # retrieved rendered container definition (included defined variables)
  requires_compatibilities = ["FARGATE"]                                           # ecs task compatible with fargate (ecs hosting without managing servers)
  network_mode             = "awsvpc"                                              # use vpcs
  cpu                      = 256                                                   # charged at different prices check docs
  memory                   = 512
  execution_role_arn       = aws_iam_role.task_execution_role.arn # permission to start a new container
  task_role_arn            = aws_iam_role.app_iam_role.arn        # permissions task has at runtime

  volume {
    name = "static"
  }

  tags = local.common_tags
}

resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS Service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  # outbound access on port 443
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound access from our service on port 5432 to our private subnet
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  # public access from internet to our proxy
  ingress {
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"
    security_groups = [
      aws_security_group.lb.id
    ]
  }
  tags = local.common_tags
}

# runs container
resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"
  cluster         = aws_ecs_cluster.main.name
  task_definition = aws_ecs_task_definition.api.family
  desired_count   = 1 # no.of tasks (instances) to run 
  #serverless docker deployment
  launch_type = "FARGATE" # run containers without managing servers

  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_a.id
    ]
    security_groups = [aws_security_group.ecs_service.id]
  }

  # register new tasks with our target group
  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy" #container to forward to
    container_port   = 8000
  }

}

