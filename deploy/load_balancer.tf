resource "aws_lb" "api" {
  name               = "${local.prefix}-main"
  load_balancer_type = "application" # handle http(s) requests
  subnets = [                        # sits in the public internet
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id]
  tags            = local.common_tags
}

# target group -> servers the lb can forward requests to
# add ecs to this target group
resource "aws_lb_target_group" "api" {
  name        = "${local.prefix}-api"
  protocol    = "HTTP" # protocol lb fowards requests using
  vpc_id      = aws_vpc.main.id
  target_type = "ip" # will add targets as ips to ecs services
  port        = 8000

  health_check {
    path = "/admin/login"
  }
}

# listener -> accepts request to our lb (entrypoint)
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  port              = 80 # port to accept requests on
  protocol          = "HTTP"

  # how to handle requests to our listener
  # pass on request to target group
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}

resource "aws_security_group" "lb" {
  description = "Allow access to application load balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  # allow all access from internet into our LB on port 80
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow lb to access proxy
  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags

}