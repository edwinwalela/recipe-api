#!/bin/sh

set -e # exit script if an error occurs

# collects all django static files to a particular directory
python manage.py collectstatic --noinput
 
# will wait for db to startup
python manage.py wait_for_db

# run migrations
python manage.py migrate

# socket runs on port 9000. Map requrest from proxy to this port
# workers no.of workers to run (based on mem and resources required)
# master - run as master service on terminal 
# enable-threads - enable multithreading
# module - application that uwsgi will to run (provided by django by default)
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi